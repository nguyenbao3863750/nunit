﻿using NUnit.Framework;
using Prime.Services;

namespace Prime.UnitTests.Services
{
    /// <summary>
    /// Thuộc tính [TestFixture] biểu thị một class chứa các unit test. 
    /// </summary>
    [TestFixture]
    public class PrimeService_IsPrimeShould
    {
        private PrimeService _primeService;

        [SetUp]
        public void SetUp()
        {
            _primeService = new PrimeService();
        }

        /// <summary>
        /// Thuộc tính [TestCase] biểu thị cho các test case, truyền vào các giá trị để test
        /// Hàm IsPrime_InputIs1_ReturnFalse() được xây dựng để test hàm IsPrime() bên PrimeSerice project
        /// </summary>
        /// <param name="value"></param>
        [TestCase(0)] // 0, 1 ,2 là các giá trị truyền vào để test
        [TestCase(1)]
        [TestCase(2)]
        public void IsPrime_InputIs1_ReturnFalse(int value)
        {
            var result = _primeService.IsPrime(value);

            Assert.IsFalse(result, "1 isn't prime"); //Mong đợi kết quả trả về của hàm IsPrime() là false
            //Nếu kết quả trả về khác false thì sẽ là failed testcase
        }

    }
}